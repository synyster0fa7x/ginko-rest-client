<?php

namespace GinkoAPI\Entity;

class Price extends BaseEntity
{
    
    /**
     * @var integer
     * @mappedBy type
     */
    protected $type;
    
    /**
     * @var string
     * @mappedBy libelle
     */
    protected $name;
    
    /**
     * @var string
     * @mappedBy description
     */
    protected $description;
    
    /**
     * @var string
     * @mappedBy pointsDeVente
     */
    protected $sellPoints;
    
    /**
     * @var float
     * @mappedBy tarif
     */
    protected $price;
}
