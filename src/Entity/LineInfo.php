<?php

namespace GinkoAPI\Entity;

class LineInfo extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy idLigne
     */
    protected $lineId;
    
    /**
     * @var string
     * @mappedBy numLignePublic
     */
    protected $linePublicId;
    
    /**
     * @var string
     * @mappedBy couleurFond
     */
    protected $backgroundColor;
    
    /**
     * @var string
     * @mappedBy couleurTexte
     */
    protected $textColor;
}
