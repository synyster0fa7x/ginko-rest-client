<?php

namespace GinkoAPI\Entity;

class WaitingTime extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy idArret
     */
    protected $stopId;
    
    /**
     * @var float
     * @mappedBy latitude
     */
    protected $latitude;
    
    /**
     * @var float
     * @mappedBy longitude
     */
    protected $longitude;
    
    /**
     * @var string
     * @mappedBy idLigne
     */
    protected $lineId;
    
    /**
     * @var string
     * @mappedBy numLignePublic
     */
    protected $linePublicId;
    
    /**
     * @var string
     * @mappedBy couleurFond
     */
    protected $backgroundColor;
    
    /**
     * @var string
     * @mappedBy couleurTexte
     */
    protected $textColor;
    
    /**
     * @var boolean
     * @mappedBy sensAller
     */
    protected $rightWay;
    
    /**
     * @var string
     * @mappedBy destination
     */
    protected $destination;
    
    /**
     * @var string
     * @mappedBy precisionDestination
     */
    protected $detail;
    
    /**
     * @var string
     * @mappedBy temps
     */
    protected $waitingTime;
    
    /**
     * @var boolean
     * @mappedBy fiable
     */
    protected $isReliable;
    
    /**
     * @var string
     * @mappedBy numVehicule
     */
    protected $vehicleId;
}
