<?php

namespace GinkoAPI\Entity;

class BaseEntity
{
    public function __construct($rawObj = null)
    {
        if ($rawObj) {
            $this->map($rawObj);
        }
    }

    /**
     * Map santard object to entity
     *
     * @param stdClass $object
     * @return void
     */
    public function map(\stdClass $rawObject)
    {
        foreach (get_object_vars($this) as $key => $value) {
            $reader = new \DocBlockReader\Reader(get_class($this), $key, 'property');
            $varType = $reader->getParameter('var');
            $mappedBy = $reader->getParameter('mappedBy');
            $mappedEntity = $reader->getParameter('mappedEntity');

            if ($mappedEntity) {
                $mappedEntity = 'GinkoAPI\Entity\\'.$mappedEntity;
                
                if (is_array($rawObject->{$mappedBy}) && $varType = 'array') {
                    $datas = [];
                    foreach ($rawObject->{$mappedBy} as $keyMappedBy => $value) {
                        $datas[] = new $mappedEntity($value);
                    }
                } elseif (is_array($rawObject->{$mappedBy}) && $varType != 'array') {
                    $datas = new $mappedEntity($value);
                }

                $this->{$key} = $datas;
            } else {
                $this->{$key} = $rawObject->{$mappedBy};
            }
        }
    }

    public function __get($name)
    {
        if (array_key_exists($name, get_object_vars($this))) {
            return $this->{$name};
        }
    }

}
