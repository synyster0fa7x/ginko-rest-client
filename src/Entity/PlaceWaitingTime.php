<?php

namespace GinkoAPI\Entity;

class PlaceWaitingTime extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy nomExact
     */
    protected $name;

    
    /**
     * @var array
     * @mappedBy listeTemps
     * @mappedEntity WaitingTime
     */
    protected $waitingTimes;
    
    /**
     * @var float
     * @mappedBy latitude
     */
    protected $latitude;
    
    /**
     * @var float
     * @mappedBy longitude
     */
    protected $longitude;
}
