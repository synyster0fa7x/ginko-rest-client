<?php

namespace GinkoAPI\Entity;

class Vehicle extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy num
     */
    protected $id;
    
    /**
     * @var boolean
     * @mappedBy affichageDynamique
     */
    protected $dynamicDisplay;
    
    /**
     * @var boolean
     * @mappedBy annoncesSonores
     */
    protected $soundNotifications;
    
    /**
     * @var boolean
     * @mappedBy accessiblePMR
     */
    protected $disabledCompliant;
}
