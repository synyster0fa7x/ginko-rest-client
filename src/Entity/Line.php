<?php

namespace GinkoAPI\Entity;

class Line extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy id
     */
    protected $id;
    
    /**
     * @var string
     * @mappedBy numLignePublic
     */
    protected $publicId;
    
    /**
     * @var boolean
     * @mappedBy modeTransport
     */
    protected $isTramway;
    
    /**
     * @var string
     * @mappedBy couleurFond
     */
    protected $backgroundColor;
    
    /**
     * @var string
     * @mappedBy couleurTexte
     */
    protected $textColor;
    
    /**
     * @var array
     * @mappedBy variantes
     * @mappedEntity Variant
     */
    protected $variants;
    
    /**
     * @var boolean
     * @mappedBy periurbain
     */
    protected $periurban;
}
