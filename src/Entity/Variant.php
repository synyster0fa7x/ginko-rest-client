<?php

namespace GinkoAPI\Entity;

class Variant extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy id
     */
    protected $id;
    
    /**
     * @var boolean
     * @mappedBy sensAller
     */
    protected $rightWay;
    
    /**
     * @var string
     * @mappedBy destination
     */
    protected $destination;
    
    /**
     * @var string
     * @mappedBy precisionDestination
     */
    protected $detail;
}
