<?php

namespace GinkoAPI\Entity;

class TrafficInfo extends BaseEntity
{
    
    /**
     * @var integer
     * @mappedBy id
     */
    protected $id;
    
    /**
     * @var string
     * @mappedBy titre
     */
    protected $title;
    
    /**
     * @var string
     * @mappedBy texte
     */
    protected $content;
    
    /**
     * @var string
     * @mappedBy complement
     */
    protected $details;
    
    /**
     * @var string
     * @mappedBy url
     */
    protected $url;

    /**
     * @var boolean
     * @mappedBy tempsReel
     */
    protected $isRealTime;

    /**
     * @var boolean
     * @mappedBy alerte
     */
    protected $isAlert;

    /**
     * @var boolean
     * @mappedBy push
     */
    protected $isPush;

    /**
     * @var array
     * @mappedBy lignes
     * @mappedEntity LineInfo
     */
    protected $lines;
    
    
}
