<?php

namespace GinkoAPI\Entity;

class Stop extends BaseEntity
{
    
    /**
     * @var string
     * @mappedBy id
     */
    protected $id;
    
    /**
     * @var boolean
     * @mappedBy nom
     */
    protected $name;
    
    /**
     * @var float
     * @mappedBy latitude
     */
    protected $latitude;
    
    /**
     * @var float
     * @mappedBy longitude
     */
    protected $longitude;
}
