<?php

namespace GinkoAPI;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use GinkoAPI\Entity\Price;
use GinkoAPI\Entity\Line;
use GinkoAPI\Entity\Variant;
use GinkoAPI\Entity\Stop;
use GinkoAPI\Entity\Vehicle;
use GinkoAPI\Entity\WaitingTime;
use GinkoAPI\Entity\PlaceWaitingTime;
use GinkoAPI\Entity\TrafficInfo;

/**
 * GinkoClient class
 *
 */
class Client
{
    private $client = null;
    private $api_url = "https://api.ginko.voyage/";
    private $config = [];
    
    const VERSION = '1.0.1';

    public function __construct($config = [])
    {
        $this->client = new GuzzleClient([
            'base_uri' => $this->api_url
        ]);

        $this->config = $config;
    }
    
    /**
     * Make Guzzle request
     *
     * @param string $method
     * @param array $params
     * @return $content
     */
    private function call(string $method, $params = [])
    {
        if ($this->config['api_key']) {
            $params['apiKey'] = $this->config['api_key'];
        }

        $response = $this->client->request('POST', $method.'.do', [
            'query' => $params,
        ]);

        if ($response->getBody()) {
            $content = json_decode($response->getBody()->getContents());

            if ($content->ok) {
                return $content->objets;
            } else {
                throw new Exception($content->message);
            }
        }
        
        throw new Exception('No response');
    }

    /**
     * Get prices
     *
     * @see https://api.ginko.voyage/#tarifs
     * @return array
     */
    public function getPrices()
    {
        $response = $this->call('Tarifs/get');
        $datas = [];
        
        foreach ($response as $rawObj) {
            $datas[] = new Price($rawObj);
        }
        
        return $datas;
    }

    /**
     * Get all lines
     *
     * @see https://api.ginko.voyage/#dr
     * @return array
     */
    public function getLines()
    {
        $response = $this->call('DR/getLignes');
        $datas = [];

        foreach ($response as $rawObj) {
            $datas[] = new Line($rawObj);
        }
        
        return $datas;
    }
    
    /**
     * Get all variant details from line ID and variant ID
     *
     * @see https://api.ginko.voyage/#dr
     * @param string $lineId
     * @param string $variantId
     * @return array
     */
    public function getVariantDetails(string $lineId, string $variantId)
    {
        $response = $this->call('DR/getDetailsVariante', [
            'idLigne' => $lineId,
            'idVariant' => $variantId
        ]);
        $datas = [];

        foreach ($response as $rawObj) {
            $datas[] = new Stop($rawObj);
        }
        
        return $datas;
    }
    
    /**
     * Get all stops
     *
     * @see https://api.ginko.voyage/#dr
     * @return array
     */
    public function getStops()
    {
        $response = $this->call('DR/getArrets');
        $datas = [];

        foreach ($response as $rawObj) {
            $datas[] = new Stop($rawObj);
        }
        
        return $datas;
    }
    
    /**
     * Get nearest stops from GPS coordinates
     *
     * @see https://api.ginko.voyage/#dr
     * @param float $latitude
     * @param float $longitude
     * @return array
     */
    public function getNearest(float $latitude, float $longitude)
    {
        $response = $this->call('DR/getArretsProches', [
            'latitude' => $latitude,
            'longitude' => $longitude
        ]);
        $datas = [];

        foreach ($response as $rawObj) {
            $datas[] = new Stop($rawObj);
        }
        
        return $datas;
    }

    /**
     * Return stop from id
     *
     * @see https://api.ginko.voyage/#dr
     * @param string $id
     * @return Stop
     */
    public function getStop(string $id)
    {
        $rawObj = $this->call('DR/getDetailsArret', [
            'id' => $id
        ]);
        
        return new Stop($rawObj);
    }
    
    /**
     * Return vehicle from id
     *
     * @see https://api.ginko.voyage/#dr
     * @param string $id
     * @return Vehicle
     */
    public function getVehicle(string $id)
    {
        $rawObj = $this->call('DR/getDetailsVehicule', [
            'num' => $id
        ]);
        
        return new Vehicle($rawObj);
    }
    
    /**
     * Return waiting times from location name
     *
     * @see https://api.ginko.voyage/#tr
     * @param string $name
     * @param integer $limit
     * @return array
     */
    public function getWaitingTimeFromLocation(string $name, int $limit = 2)
    {
        $rawObj = $this->call('TR/getTempsLieu', [
            'nom' => $name,
            'nb' => $limit,
        ]);
        
        return new PlaceWaitingTime($rawObj);
    }
    
    /**
     * Return traffic alerts
     *
     * @see https://api.ginko.voyage/#tr
     * @return array
     */
    public function getTrafficAlerts()
    {
        $rawObj = $this->call('TR/getInfosTrafic');

        $datas = [
            'anticipated' => [],
            'realTime' => []
        ];
        foreach ($rawObj->infosAnticipees as $key => $value) {
            $datas['anticipated'][] = new TrafficInfo($value);
        }
        
        foreach ($rawObj->infosTempsReel as $key => $value) {
            $datas['realTime'][] = new TrafficInfo($value);
        }
        
        return $datas;
    }

    /**
     * Undocumented function
     *
     * @param array $search
     * @param integer $limit
     * @param boolean $keepOrder
     * @return array
     */
    public function getWaitingTimeList(array $searches, int $limit = 2, $keepOrder = false) 
    {
        $query = [
            'preserverOrdre' => $keepOrder 
        ];

        foreach ($searches as $search) {
            foreach ($search as $key => $value) {
                switch ($key) {
                    case 'name':
                        $query['listeNoms'][] = $value;
                        break;
                    
                    case 'lineId':
                        $query['listeIdLignes'][] = $value;
                        break;
                    
                    case 'rightWay':
                        $query['listeSensAller'][] = $value;
                        break;
                    
                    default:
                        break;
                }
            }
        }

        foreach ($query as $key => $value) {
            if (is_array($value)) {
                $query[$key] = implode('~', $value);
            }
        }

        $response = $this->call('TR/getListeTemps', $query);

        $datas = [];

        foreach ($response as $rawObj) {
            $datas[] = new PlaceWaitingTime($rawObj);
        }

        return $datas;
    }
}
