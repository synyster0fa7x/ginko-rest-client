Ginko Rest API Client
====================

[![CodeFactor](https://www.codefactor.io/repository/github/synyster0fa7x/ginko-rest-client/badge/master)](https://www.codefactor.io/repository/github/synyster0fa7x/ginko-rest-client/overview/master)

A simple client for [Ginko Rest API](http://www.ginko.voyage/)

## Install

```
composer require synyster0fa7x/ginko-rest-client
```

## Usage

```php
require_once('path/to/vendor/autoload.php');
use Ginko\GinkoClient;

$client = new GinkoClient([
    'apiKey' => 'YOUR_API_KEY'
]); 
// NOTE : api_key is optionnal until 2019

$stops = $client->getStops(); // return all bus stops

$waitingTimes = $client->getWaitingTimeList([
    [
        'name' => 'haut du chazal',
        'lineId' => '101',
        'rightWay' => 1
    ], 
    [
        'name' => 'chalezeule',
        'lineId' => '101',
        'rightWay' => 0
    ]
]);

// echo '<pre>';var_dump($waitingTimes[0]->waitingTimes);echo '</pre>';die;

//show all client method's
//echo '<pre>';var_dump(get_class_methods($client));echo '</pre>';die;
```

You can check all files in `src/Entity` directory to learn how datas are mapped from the API return. All client's methods are documented.

## API Documentation

All informations are available at [https://api.ginko.voyage/#prez](https://api.ginko.voyage/#prez)

### License

This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
